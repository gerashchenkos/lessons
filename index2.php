<?php
    require_once("config.php");
    
    if(!empty($_SESSION['logged'])){
        header("Location:/home.php");
        die();
    } elseif (!empty($_POST)) {
        if(!empty($_POST['login']) && !empty($_POST['password'])){
            if(file_exists('test.txt')){
                include_once(ROOT_PATH."/functions/check_login.php");
                if($res){
                    $_SESSION['logged'] = 1;
                    include_once(ROOT_PATH."/functions/log_success_login.php");
                }else {
                    $error = "Неверный логин или пароль!";
                }
            }else {
                $error = "Файл не найден!";
            }
        } else {
            $error = 'Please enter login and pass';
        }
    }
	
    require_once(ROOT_PATH."/views/template.php");
?>