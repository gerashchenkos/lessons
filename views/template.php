<!DOCTYPE html>
<html>
<head>
    <title>Beetroot</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class="container h-100">
        <div class="row h-100 justify-content-center align-items-center">
            <div class="col-6 mt-4">
                <?php if(!empty($error)) { ?>
                    <div class="alert alert-danger">
                        <?php echo $error; ?>
                    </div>
                <?php } ?>
                <?php if(!empty($msg)) { ?>
                    <div class="alert alert-success">
                        <?php echo $msg; ?>
                    </div>
                <?php } ?>
                <div class="card">
                    <div class="card-header">
                        <h4>Форма авторизации</h4>
                    </div>
                    <div class="p-4">
                        <form class="mt-2" enctype="multipart/form-data" method="post" action="<?php echo SITE_URL.'/index2.php';?>">
                            <div class="form-group">
                                <label><h5>Логин</h5></label>
                                <input type="text" class="form-control" name="login">
                            </div>
                            <div class="form-group">
                                <label><h5>Пароль</h5></label>
                                <input type="password" class="form-control" name="password">
                            </div>
                            <div class="form-group">
                                <label><h5>Файл</h5></label>
                                <input type="file" class="form-control" name="files">
                            </div>
                            <div class="float-right">
                                <button type="submit" class="btn btn-info">Отправить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>