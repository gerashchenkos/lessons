<?php
    require_once("config.php");
    $payload = json_encode( array( "title"=> "111", "body" => "222", "userId" => 1 ) );
    $domain = "https://jsonplaceholder.typicode.com/posts";
    $curlInit = curl_init($domain);
    curl_setopt($curlInit, CURLOPT_CONNECTTIMEOUT, 10);
    //curl_setopt($curlInit, CURLOPT_HEADER,true);
    curl_setopt($curlInit, CURLOPT_RETURNTRANSFER,true);
    curl_setopt( $curlInit, CURLOPT_POSTFIELDS, $payload );
    curl_setopt( $curlInit, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
    curl_setopt($curlInit, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curlInit, CURLOPT_SSL_VERIFYPEER, 0);

    $response = curl_exec($curlInit);
    curl_close($curlInit);
    var_dump($response);
?>